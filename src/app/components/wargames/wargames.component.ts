import { Component, OnInit } from '@angular/core';
import {WarGame} from '../../models/WarGame';
import {Subscription} from 'rxjs';
import {WarGameService} from '../../services/WarGameService';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-wargames',
  templateUrl: './wargames.component.html',
  styleUrls: ['./wargames.component.css'],
  providers: [WarGameService]
})
export class WargamesComponent implements OnInit {
  pageTitle = 'War games';
  warGames: WarGame[] = [];
  errorMessage = '';
  sub!: Subscription;
  defaultId = 0;

  constructor(public formBuildService: FormBuilder, private warGameService: WarGameService) { }

  public addWarGameForm = this.formBuildService.group({
    warGameName: ['', Validators.required],
    genre: ['', Validators.required]
  });

  ngOnInit(): void {
    this.loadWarGames();
  }

  loadWarGames(): void {
    this.sub = this.warGameService.getWarGames().subscribe({
      next: warGames => {this.warGames = warGames; console.log(this.warGames)},
      error: err => this.errorMessage = err
    });
  }

  addWarGame(event: any): void {
    const warGameName = this.addWarGameForm.controls.warGameName.value;
    const genre = this.addWarGameForm.controls.genre.value;
    console.log('name: ' + warGameName + ' , genre: ' + genre);
    const warGame = new WarGame(this.defaultId, warGameName, genre);
    console.log('Calling service with warGame: ' + warGame.genre + warGame.name);
    this.warGameService.addWarGame(warGame).subscribe(next => this.loadWarGames());
  }
}
