import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {WarGame} from '../models/WarGame';
import {catchError, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WarGameService {
  private warGamesUrl = 'http://localhost:8080/warchives/wargames';

  constructor(private http: HttpClient) {}

  getWarGames(): Observable<WarGame[]> {
    console.log('Sending a request to ' + this.warGamesUrl);
    return this.http.get<any>(this.warGamesUrl)
      .pipe(
        tap(data => console.log('All: ', JSON.stringify(data))),
        catchError(this.handleError)
      );
  }

  getWarGame(name: string): Observable<WarGame | undefined> {
    return this.http.get<WarGame>(this.warGamesUrl + name).pipe(
      catchError(this.handleError)
    );
  }

  addWarGame = (warGame: WarGame) => {
    console.log('trying to add warGame: ' + warGame + ' to ' + this.warGamesUrl);
    return this.http.post(this.warGamesUrl, warGame).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(err: HttpErrorResponse): Observable<never> {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
}
