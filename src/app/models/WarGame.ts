export class WarGame {
  id: number;
  name: string;
  genre: string;
  public constructor(
    id: number,
    name: string,
    genre: string) {
    console.log('creating war game');
    this.id = id;
    this.name = name;
    this.genre = genre;
    console.log('wargame created: ' + this);
  }
}
