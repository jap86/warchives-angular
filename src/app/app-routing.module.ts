import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {WargamesComponent} from './components/wargames/wargames.component';

const routes: Routes = [
  { path: 'wargames', component: WargamesComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
